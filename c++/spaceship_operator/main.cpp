#include <iostream>
#include <compare>

struct A {
    int i;
};

struct B {
    int i;

    // constexpr auto operator<=>(const B& rhs) const noexcept = default;
    // {
    //     return i <=> rhs.i;
    // }

    B(int i_in)
        : i(i_in)
    {}

    B(const A& a)
        : i(a.i)
    {}
};

// constexpr bool operator==(const B& lhs, const B& rhs) noexcept
// {
//     return lhs.i == rhs.i;
// }

constexpr auto operator<=>(const B& lhs, const B& rhs) noexcept
{
    return lhs.i <=> rhs.i;
}

int main()
{
    std::cout << (B{2} == B{2} ? "true" : "false");
    std::cout << (A{2} == B{2} ? "true" : "false");
    return 0;
}
