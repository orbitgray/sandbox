#include <benchmark/benchmark.h>

class Singleton_ptr
{
	Singleton_ptr()
		: i(0)
	{
		// std::cout << "Singleton_ptr::Singleton_ptr()\n";
	}

	volatile int i;
	static Singleton_ptr *instance;

public:
	static Singleton_ptr* GetInstance()
	{
		if (nullptr == instance)
			instance = new Singleton_ptr();
		return instance;
	}

	void Add()
	{
		i++;
	}

	~Singleton_ptr()
	{
		// std::cout << "Singleton_ptr::~Singleton_ptr()\n";
	}
};
Singleton_ptr *Singleton_ptr::instance = nullptr;

static void SingletonPtrMultiThreaded(benchmark::State& state) {
  if (state.thread_index == 0) {
	  Singleton_ptr::GetInstance()->Add();
  }
  for (auto _ : state) {
	  Singleton_ptr::GetInstance()->Add();
  }
  if (state.thread_index == 0) {
    // Teardown code here.
  }
}

static void SingletonPtr(benchmark::State& state)
{
	Singleton_ptr::GetInstance()->Add();
	for (auto _ : state)
		Singleton_ptr::GetInstance()->Add();
}
BENCHMARK(SingletonPtr);

class Singleton_ptr2
{
	Singleton_ptr2()
		: i(0)
	{
		// std::cout << "Singleton_ptr2::Singleton_ptr2()\n";
	}

	volatile int i;
	static Singleton_ptr2 *instance;

public:
	static void Init()
	{
		if (nullptr == instance)
			instance = new Singleton_ptr2();
	}

	static Singleton_ptr2* GetInstance()
	{
		return instance;
	}

	void Add()
	{
		i++;
	}

	~Singleton_ptr2()
	{
		// std::cout << "Singleton_ptr2::~Singleton_ptr2()\n";
	}
};
Singleton_ptr2 *Singleton_ptr2::instance = nullptr;

static void SingletonPtr2(benchmark::State& state)
{
	Singleton_ptr2::Init();
	for (auto _ : state)
		Singleton_ptr2::GetInstance()->Add();
}
BENCHMARK(SingletonPtr2);

static void SingletonPtr2MultiThreaded(benchmark::State& state) {
  if (state.thread_index == 0) {
	  Singleton_ptr2::Init();
  }
  for (auto _ : state) {
	  Singleton_ptr2::GetInstance()->Add();
  }
  if (state.thread_index == 0) {
    // Teardown code here.
  }
}

class Singleton_ref
{
	Singleton_ref()
		: i(0)
	{
		// std::cout << "Singleton_ref::Singleton_ref()\n";
	}

	volatile int i;

public:
	static Singleton_ref& GetInstance()
	{
		static Singleton_ref instance;
		return instance;
	}

	void Add()
	{
		i++;
	}

	~Singleton_ref()
	{
		// std::cout << "Singleton_ref::~Singleton_ref()\n";
	}
};

static void SingletonRef(benchmark::State& state)
{
	Singleton_ref::GetInstance().Add();
	for (auto _ : state)
		Singleton_ref::GetInstance().Add();
}
BENCHMARK(SingletonRef);

static void SingletonRefMultiThreaded(benchmark::State& state) {
  if (state.thread_index == 0) {
	  Singleton_ref::GetInstance().Add();
  }
  for (auto _ : state) {
	  Singleton_ref::GetInstance().Add();
  }
  if (state.thread_index == 0) {
    // Teardown code here.
  }
}

class Singleton_ref2
{
	Singleton_ref2()
		: i(0)
	{
		// std::cout << "Singleton_ref2::Singleton_ref2()\n";
	}

	volatile int i;
	static Singleton_ref2 instance;

public:
	static Singleton_ref2& GetInstance()
	{
		return instance;
	}

	void Add()
	{
		i++;
	}

	~Singleton_ref2()
	{
		// std::cout << "Singleton_ref2::~Singleton_ref2()\n";
	}
};

Singleton_ref2 Singleton_ref2::instance;

static void SingletonRef2(benchmark::State& state)
{
	Singleton_ref2::GetInstance().Add();
	for (auto _ : state)
		Singleton_ref2::GetInstance().Add();
}
BENCHMARK(SingletonRef2);

static void SingletonRef2MultiThreaded(benchmark::State& state) {
  if (state.thread_index == 0) {
	  Singleton_ref2::GetInstance().Add();
  }
  for (auto _ : state) {
	  Singleton_ref2::GetInstance().Add();
  }
  if (state.thread_index == 0) {
    // Teardown code here.
  }
}


BENCHMARK(SingletonPtrMultiThreaded)->Threads(1);
BENCHMARK(SingletonPtr2MultiThreaded)->Threads(1);
BENCHMARK(SingletonRefMultiThreaded)->Threads(1);
BENCHMARK(SingletonRef2MultiThreaded)->Threads(1);

BENCHMARK(SingletonPtrMultiThreaded)->Threads(2);
BENCHMARK(SingletonPtr2MultiThreaded)->Threads(2);
BENCHMARK(SingletonRefMultiThreaded)->Threads(2);
BENCHMARK(SingletonRef2MultiThreaded)->Threads(2);

BENCHMARK(SingletonPtrMultiThreaded)->Threads(4);
BENCHMARK(SingletonPtr2MultiThreaded)->Threads(4);
BENCHMARK(SingletonRefMultiThreaded)->Threads(4);
BENCHMARK(SingletonRef2MultiThreaded)->Threads(4);

BENCHMARK_MAIN();
