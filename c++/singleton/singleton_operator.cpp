#include <iostream>

// doesn't build
// no static operator allowed

class Singleton_1
{
	Singleton_1()
	{
		std::cout << "Singleton_1::Singleton_1()\n";
	}

public:
	static Singleton_1& operator()()
	{
		static Singleton_1 instance;
		return instance;
	}

	void Print()
	{
		std::cout << "Singleton_1::Print()\n";
	}

	~Singleton_1()
	{
		std::cout << "Singleton_1::~Singleton_1()\n";
	}
};

int main()
{
	Singleton_1().Print();
	Singleton_1().Print();
	return 0;
}
