#include <iostream>

class Singleton_ref
{
	Singleton_ref()
	{
		std::cout << "Singleton_ref::Singleton_ref()\n";
	}

public:
	static Singleton_ref& GetInstance()
	{
		static Singleton_ref instance;
		return instance;
	}

	void Print()
	{
		std::cout << "Singleton_ref::Print()\n";
	}

	~Singleton_ref()
	{
		std::cout << "Singleton_ref::~Singleton_ref()\n";
	}
};

int main()
{
	Singleton_ref::GetInstance().Print();
	Singleton_ref::GetInstance().Print();
	return 0;
}
