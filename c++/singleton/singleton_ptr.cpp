#include <iostream>

class Singleton_ptr
{
	Singleton_ptr()
	{
		std::cout << "Singleton_ptr::Singleton_ptr()\n";
	}

	static Singleton_ptr *instance;

public:
	static Singleton_ptr* GetInstance()
	{
		if (nullptr == instance)
			instance = new Singleton_ptr();
		return instance;
	}

	void Print()
	{
		std::cout << "Singleton_ptr::Print()\n";
	}

	~Singleton_ptr()
	{
		std::cout << "Singleton_ptr::~Singleton_ptr()\n";
	}
};
Singleton_ptr *Singleton_ptr::instance = nullptr;

int main()
{
	Singleton_ptr::GetInstance()->Print();
	Singleton_ptr::GetInstance()->Print();
	return 0;
}
