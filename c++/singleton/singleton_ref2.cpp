#include <iostream>

class Singleton_ref
{
	Singleton_ref()
	{
		std::cout << "Singleton_ref::Singleton_ref()\n";
	}

	static Singleton_ref instance;

public:
	static Singleton_ref& GetInstance()
	{
		return instance;
	}

	void Print()
	{
		std::cout << "Singleton_ref::Print()\n";
	}

	~Singleton_ref()
	{
		std::cout << "Singleton_ref::~Singleton_ref()\n";
	}
};

Singleton_ref Singleton_ref::instance;

int main()
{
        Singleton_ref::GetInstance().Print();
        Singleton_ref::GetInstance().Print();
        return 0;
}

