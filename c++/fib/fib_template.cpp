#include <iostream>

template <int V>
struct fib
{
	static constexpr int val = fib<V-1>::val + fib<V-2>::val; 
};

template <>
struct fib<0>
{
	static constexpr int val = 0; 
};

template <>
struct fib<1>
{
	static constexpr int val = 1; 
};

int main()
{

	std::cout << fib<40>::val << '\n';
	return 0;
}
