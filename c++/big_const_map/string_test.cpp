#include <map>
#include <string>
#include <iostream>

const std::map<uint32_t, const std::string> const_map = {
	{0x00E043DB, "00E043DB"},
	{0x002405F5, "002405F5"},
};

int main()
{
    auto it = const_map.find(0x00288088);
    std::cout << it->second << std::endl;
    return 0;
}
