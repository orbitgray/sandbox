#include <HsFFI.h>
#include "Test_import_stub.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	hs_init(&argc, &argv);

	{
		int *arr = (int*)haskell_test();
		printf("from haskell:\n");
		for (int i = 0; i < 3; i++)
			printf("arr[%i] = %i\n", i, arr[i]);
	}

	hs_exit();
	return 0;
}
