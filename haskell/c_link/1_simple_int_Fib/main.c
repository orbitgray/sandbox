#include <HsFFI.h>
#include "Fib_import_stub.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	hs_init(&argc, &argv);

	if (argc != 2)
	{
		printf("Please provide number to calculate fib on.\n");
		exit(1);
	}

	{
		int val = atoi(argv[1]);
		int i;
		i = fibonacci_hs(val);
		printf("Fibonacci: %d\n", i);
	}

	hs_exit();
	return 0;
}
