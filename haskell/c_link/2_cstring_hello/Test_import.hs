module Test_import where

import Foreign.C.String
import Foreign.C.Types

haskell_test :: IO CString
haskell_test = newCString "hello world";

foreign export ccall haskell_test :: IO CString
