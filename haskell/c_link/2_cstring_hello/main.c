#include <HsFFI.h>
#include "Test_import_stub.h"

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	hs_init(&argc, &argv);

	{
		printf("from haskell: '%s'\n", (char*)haskell_test());
	}

	hs_exit();
	return 0;
}
