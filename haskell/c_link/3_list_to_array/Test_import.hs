module Test_import where

import Foreign.Ptr
import Foreign.Marshal.Array
import Foreign.C.Types

haskell_test :: IO (Ptr CInt)
haskell_test = newArray (map (fromIntegral) [1, 2, 5])

foreign export ccall haskell_test :: IO (Ptr CInt)
